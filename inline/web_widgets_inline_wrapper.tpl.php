<?php
/**
 * @file
 * Inline embedding means that we include the views output into a javascript file
 */
?>
window.onload = function() {
  document.getElementById(widgetContext['widgetid']).innerHTML = <?php print $js_string ?>;
};
