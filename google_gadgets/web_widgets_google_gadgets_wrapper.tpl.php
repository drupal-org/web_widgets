<?php
/**
 * @file
 * Inline embedding means that we use the views output only
 */
?>
<?php print '<?xml version="1.0" encoding="UTF-8" ?>' ?>
<Module>
  <ModulePrefs title="<?php print $title ?>" />
  <Content type="html">
     <![CDATA[ 
<?php print $content ?>
     ]]>
  </Content> 
</Module>
