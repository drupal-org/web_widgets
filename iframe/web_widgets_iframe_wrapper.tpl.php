<?php
/**
 * @file
 * HTML wrapper code for the iframe'ed code at the external site.
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><?php print $title ?></title>
  </head>
  <body>
  <?php print $content ?>
  </body>
</html>
