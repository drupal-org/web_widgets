<?php
/**
 * @file
 * Iframe-specific functions, preprocessing
 */

/**
 * Define the theme functions for the web_widgets module
 */
function web_widgets_style_iframe() {
  $definition = array(
    'theme' => array(
      // The HTML-Javascript code lines what should be pasted into the external site
      'web_widgets_iframe' => array(
        'template' => 'web_widgets_iframe',
        'arguments' => array('path' => NULL, 'width' => NULL, 'height' => NULL),
        'path' => drupal_get_path('module', 'web_widgets') .'/iframe',
      ),
      // The html page which is used inside the iframe, usually will show up on an external site
      'web_widgets_iframe_wrapper' => array(
        'template' => 'web_widgets_iframe_wrapper',
        'arguments' => array('content' => NULL, 'title' => NULL),
        'path' => drupal_get_path('module', 'web_widgets') .'/iframe',
      ),
    ),
    'human_readable' => t('IFrame'),
  );
  return $definition;
}

/**
 * Process variables for web_widgets_iframe.tpl.php
 */
function template_preprocess_web_widgets_iframe(&$variables) {
  static $id = 0;
  $variables['wid'] = 'web_widget_iframe_'. $id++;

  // Create Javascript variables
  $widgetcontext = new stdClass();
  $widgetcontext->url = $variables['path'];
  if (empty($variables['width'])) {
    $variables['width'] = 200;
  }
  if (empty($variables['height'])) {
    $variables['height'] = 200;
  }

  $widgetcontext->width = $variables['width'];
  $widgetcontext->height = $variables['height'];
  $widgetcontext->widgetid = $variables['wid'];

  $variables['js_variables'] = drupal_to_js($widgetcontext);
  $variables['script_url'] = $GLOBALS['base_url'] .'/'. drupal_get_path('module', 'web_widgets') .'/iframe/web_widgets_iframe.js';
}
