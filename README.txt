
Web Widgets
===========

"Copy/paste widgets for your Drupal site. Plays nice with Views."

Description
===========

Use Web Widgets to build dynamic snippets of your website that users can copy 
and paste to their websites.

Installation and Usage
======================

Web widgets can be used by any other module to generate copy/paste widgets. 
However, the most common use case is to use it together with Views module.

Here is how to use Web Widgets with Views: 

* Install Views http://drupal.org/project/views.
* Install Web Widgets.
* Create a new view, add and configure a Page display, add and configure a 
  Widget display.
* Attach the widget to the page display. When you go to the page, you should 
  see a textfield with code that can be embedded on other websites.